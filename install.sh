#!/bin/zsh

# Reset
Reset='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

i=1

spinner()
{
	local pid=$1
	local delay=0.5
	local spinstr='|/-\'
	while [ "$(ps -A 2>/dev/null | awk '{print $1}' | grep $pid)" ]; do
		tput cup $i 0
		local temp=${spinstr#?}
		echo -n "[${temp:0:1}]"
		local spinstr=$temp${spinstr%"$temp"}
		sleep $delay
	done
	tput cup $((i)) 0
	if [[ "$?" = '0' ]]; then
		echo ${Green}'Done'${Reset}
	else
		echo ${Red}'Failed'${Reset}
	fi
	i=$((i+2))
}

clear

(sudo apt-get install -y git > /dev/null 2>&1) &
echo ${Cyan}'Installing git'${Reset}
spinner $!
(sudo apt-get install -y vim > /dev/null 2>&1) &
echo ${Cyan}'Installing vim'${Reset}
spinner $!
(sudo apt-get install -y lirc > /dev/null 2>&1;\
	sudo echo 'lirc_dev' | sudo tee -a /etc/modules;\
	sudo echo 'dtoverlay=lirc-rpi,gpio_in_pin=23,gpio_out_pin=22' | sudo tee -a /boot/config.txt;\
	curl 'http://pastebin.com/raw/MKA1KaLS' | sudo tee -a /etc/lirc/lircd.conf;\
	sudo echo 'DRIVER="default"' | sudo tee -a /etc/lirc/hardware.conf;\
	sudo echo 'DEVICE="/dev/lirc0"' | sudo tee -a /etc/lirc/hardware.conf;\
	sudo echo 'MODULES="lirc_rpi"' | sudo tee -a /etc/lirc/hardware.conf) &
echo ${Cyan}'Installing lirc'${Reset}
spinner $!
(curl -sSL https://get.docker.com | sh > /dev/null 2>&1; sudo usermod -aG docker pi) &
echo ${Cyan}'Installing docker'${Reset}
spinner $!
